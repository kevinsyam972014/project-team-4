<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPostsIdToLikeKomentarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('like_komentar', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('komentar_id')->nullable();
            $table->foreign('komentar_id')->references('id')->on('komentar');
            $table->unsignedBigInteger('users_id')->nullable();
            $table->foreign('users_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('like_komentar', function (Blueprint $table) {
            //
            $table->dropForeign(['users_id']);
            $table->dropColumn(['users_id']);
            $table->dropForeign(['komentar_id']);
            $table->dropColumn(['komentar_id']);
        });
    }
}
