<header>
    <div class="container">
        <div class="header-data">
            <div class="search-bar">
                <form>
                    <input type="text" name="search" placeholder="Search...">
                    <button type="submit"><i class="la la-search"></i></button>
                </form>
            </div>
            <!--search-bar end-->
            <nav>
                <ul>
                    <li>
                        <a href="/beranda" title="">
                            <span><img src="images/icon1.png" alt=""></span>
                            Home
                        </a>
                    </li>
                 
                    <li>
                        <a href="/profile" title="">
                            <span><img src="images/icon4.png" alt=""></span>
                            Profiles
                        </a>
                       
                    </li>
        
                </ul>
            </nav>
            <!--nav end-->
            <div class="menu-btn">
                <a href="#" title=""><i class="fa fa-bars"></i></a>
            </div>
            <!--menu-btn end-->
            <div class="user-account">
                <div class="user-info">
                    <img src="http://via.placeholder.com/30x30" alt="">
                    <a href="#" title="">John</a>
                    <i class="la la-sort-down"></i>
                </div>
                <div class="user-account-settingss">
                    <h3>Setting</h3>
                    <ul class="us-links">
                        <li><a href="profile-account-setting.html" title="">Account Setting</a></li>    
                    </ul>
                    <h3 class="tc"><a href="sign-in.html" title="">Logout</a></h3>
                </div>
                <!--user-account-settingss end-->
            </div>
        </div>
        <!--header-data end-->
    </div>
</header>